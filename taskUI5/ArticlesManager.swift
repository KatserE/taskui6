//
//  ArticleSaver.swift
//  taskUI5
//
//  Created by Евгений Кацер on 29.05.2023.
//

import Foundation

class ArticlesManager {
    private static let keyForArticles = "savedArticles"
    static var saved = {
        guard let stored = UserDefaults.standard.data(forKey: keyForArticles),
              let decoded = try? NSKeyedUnarchiver.unarchivedArrayOfObjects(ofClass: Article.self, from: stored)
        else {
            return [Article]()
        }
        
        return decoded
    }()
    
    static var lastAdded: Article? = nil
    
    static func save(article: Article) {
        saved.append(article)
        lastAdded = article
        
        let archived = try? NSKeyedArchiver.archivedData(
            withRootObject: saved, requiringSecureCoding: false
        )
        
        if let archived {
            UserDefaults.standard.set(archived, forKey: keyForArticles)
        }
    }
}
