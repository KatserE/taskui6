//
//  ApiResponse.swift
//  taskUI5
//
//  Created by Евгений Кацер on 22.05.2023.
//

final class ApiResponse : Codable {
    let status: String
    let articles: [Article]
    
    enum CodingKeys: String, CodingKey {
        case status
        case articles = "results"
    }
}
