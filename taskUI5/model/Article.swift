//
//  Article.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

final class Article : NSObject, NSCoding, NSSecureCoding, Codable {
    static let supportsSecureCoding = true

    let source: String
    let date: String
    let section: String
    let author: String
    let title: String
    let abstract: String
    let media: [Media]

    var image: UIImage? = nil

    enum CodingKeys: String, CodingKey {
        case source
        case date = "published_date"
        case section
        case author = "byline"
        case title
        case abstract
        case media
    }

    required init?(coder: NSCoder) {
        source = coder.decodeObject(forKey: CodingKeys.source.rawValue) as! String
        date = coder.decodeObject(forKey: CodingKeys.date.rawValue) as! String
        section = coder.decodeObject(forKey: CodingKeys.section.rawValue) as! String
        author = coder.decodeObject(forKey: CodingKeys.author.rawValue) as! String
        title = coder.decodeObject(forKey: CodingKeys.title.rawValue) as! String
        abstract = coder.decodeObject(forKey: CodingKeys.abstract.rawValue) as! String
        media = coder.decodeArrayOfObjects(
            ofClass: Media.self, forKey: CodingKeys.media.rawValue
        ) ?? []

        image = coder.decodeObject(forKey: "image") as! UIImage?
    }

    func encode(with coder: NSCoder) {
        coder.encode(source, forKey: CodingKeys.source.rawValue)
        coder.encode(date, forKey: CodingKeys.date.rawValue)
        coder.encode(section, forKey: CodingKeys.section.rawValue)
        coder.encode(author, forKey: CodingKeys.author.rawValue)
        coder.encode(title, forKey: CodingKeys.title.rawValue)
        coder.encode(abstract, forKey: CodingKeys.abstract.rawValue)
        coder.encode(media, forKey: CodingKeys.media.rawValue)

        coder.encode(image, forKey: "image")
    }

    static func ==(lhs: Article, rhs: Article) -> Bool {
        return lhs.source == rhs.source &&
               lhs.date == rhs.date &&
               lhs.section == rhs.section &&
               lhs.author == rhs.author &&
               lhs.title == rhs.title
    }

    override var hash: Int { abstract.hashValue }

    override func isEqual(_ object: Any?) -> Bool {
        guard let article = object as? Article else { return false }

        return self === article || self == article
    }
}
