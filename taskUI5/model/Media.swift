//
//  Media.swift
//  taskUI5
//
//  Created by Евгений Кацер on 22.05.2023.
//

import Foundation

final class MediaMetadata : NSObject, NSCoding, NSSecureCoding, Codable {
    static let supportsSecureCoding = true
    
    let url: String
    let height: Int
    let width: Int
    
    init?(coder: NSCoder) {
        url = coder.decodeObject(forKey: "url") as? String ?? ""
        height = coder.decodeInteger(forKey: "height")
        width = coder.decodeInteger(forKey: "width")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(url, forKey: "url")
        coder.encode(height, forKey: "height")
        coder.encode(width, forKey: "width")
    }
}

final class Media : NSObject, NSCoding, NSSecureCoding, Codable {
    static let supportsSecureCoding = true
    
    let type: String
    let metadatas: [MediaMetadata]
    
    enum CodingKeys: String, CodingKey {
        case type
        case metadatas = "media-metadata"
    }
    
    init?(coder: NSCoder) {
        type = coder.decodeObject(forKey: CodingKeys.type.rawValue) as? String ?? ""
        
        metadatas = coder.decodeArrayOfObjects(
            ofClass: MediaMetadata.self, forKey: CodingKeys.metadatas.rawValue
        ) ?? []
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(type, forKey: CodingKeys.type.rawValue)
        coder.encode(metadatas, forKey: CodingKeys.metadatas.rawValue)
    }
}
