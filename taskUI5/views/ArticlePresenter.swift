//
//  ArticlePresenter.swift
//  taskUI5
//
//  Created by Евгений Кацер on 28.05.2023.
//

import UIKit

protocol FavouriteAddDelegate {
    func wasAddedToFavourites(article: Article)
}

class ArticlePresenter: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static let detailInfoID = "detailInfoID"
    
    weak var table: UITableView!
    var articles = [Article]()
    
    func initPresentation(tableView: UITableView!) {
        table = tableView
        
        table.delegate = self
        table.dataSource = self
        table.register(cell: ArticleTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ArticleTableViewCell.getID(),
            for: indexPath
        ) as! ArticleTableViewCell
        
        let article = articles[indexPath.row]
        cell.configure(title: article.title, author: article.author)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = articles[indexPath.row]
        performSegue(withIdentifier: ArticlePresenter.detailInfoID, sender: article)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ArticlePresenter.detailInfoID, let article = sender as? Article {
            let destination = segue.destination as! ArticleDetailsViewController
            destination.articleToShow(article)
        }
    }
}
