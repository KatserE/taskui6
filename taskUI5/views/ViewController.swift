//
//  ViewController.swift
//  taskUI5
//
//  Created by Евгений Кацер on 28.05.2023.
//

import UIKit

class ViewController: UITabBarController {
    private static let errorDisplayerID = "errorDisplayerID"
    
    private var networkManager: NetworkManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var url: URL = URL.picturesDirectory
        do {
            url = try URLMaker.makeURL(request: "https://api.nytimes.com/svc/mostpopular/v2/viewed/30.json")
        } catch URLMakerError.CantLoadAPIKey { // If go to any catch => exit(0)
            wasOccuredError("Can't find a file with the api-key!")
        } catch URLMakerError.UncorrectURL {
            wasOccuredError("Can't connect to server!")
        } catch {
            wasOccuredError("Something wrong!")
        }
        
        networkManager = NetworkManager(url: url)
        networkManager!.obtainArticles { [weak self] result in
            switch result {
            case .success(let recivedArticles):
                self?.dataWasLoaded(atricles: recivedArticles)
            case .failure(let error):
                DispatchQueue.main.async {
                    let errorMsg: String
                    
                    switch error {
                    case .CantConnectToServer: errorMsg = "Can't connect to server!"
                    case .ServerDontSendData: errorMsg = "Server didn't send data!"
                    case .CantParseData: errorMsg = "Can't parse data!"
                    }
                    
                    self?.wasOccuredError(errorMsg)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.errorDisplayerID, let errorMsg = sender as? String {
            let destination = segue.destination as! ErrorDisplayerViewController
            destination.messageToShow(errorMsg)
        }
    }
    
    private func wasOccuredError(_ errorMsg: String) {
        performSegue(withIdentifier: ViewController.errorDisplayerID, sender: errorMsg)
    }
    
    private func dataWasLoaded(atricles: [Article]) {
        let navigation = viewControllers![0] as! UINavigationController
        let articlesPresenter = navigation.topViewController as! NetArticlesPresenter
        
        articlesPresenter.showArticles(atricles)
    }
}
