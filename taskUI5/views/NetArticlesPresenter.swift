//
//  AllArticlesPresenter.swift
//  taskUI5
//
//  Created by Евгений Кацер on 28.05.2023.
//

import UIKit

class NetArticlesPresenter: ArticlePresenter, FavouriteAddDelegate {
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        initPresentation(tableView: tableView)
    }
    
    func showArticles(_ articles: [Article]) {
        self.articles = Array(Set(articles).subtracting(ArticlesManager.saved))
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ArticlePresenter.detailInfoID, let article = sender as? Article {
            let destination = segue.destination as! ArticleDetailsViewController
            destination.articleToShow(article, showAddToFavouritesButton: true)
            destination.delegate = self
        }
    }
    
    func wasAddedToFavourites(article: Article) {
        articles.removeAll { $0 == article }
        DispatchQueue.main.async {[weak self] in
            self?.table.reloadData()
        }
    }
}
