//
//  StoredArticlesPresenter.swift
//  taskUI5
//
//  Created by Евгений Кацер on 28.05.2023.
//

import UIKit

class StoredArticlesPresenter: ArticlePresenter {
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initPresentation(tableView: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        articles = ArticlesManager.saved
        tableView.reloadData()
    }
}
