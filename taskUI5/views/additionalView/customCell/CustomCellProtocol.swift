//
//  CustomCellProtocol.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

protocol CustomCell {
    static func getID() -> String
    static func getNib() -> UINib
}

extension CustomCell {
    static func getID() -> String {
        String(describing: self)
    }
    
    static func getNib() -> UINib {
        UINib(nibName: getID(), bundle: nil)
    }
}
