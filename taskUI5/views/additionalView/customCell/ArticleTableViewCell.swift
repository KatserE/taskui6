//
//  ArticleTableViewCell.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

class ArticleTableViewCell: UITableViewCell, CustomCell {
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var author: UILabel!
    
    func configure(title: String, author: String) {
        self.title.text = title
        self.author.text = author
    }
}
