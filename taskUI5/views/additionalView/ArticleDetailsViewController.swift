//
//  ArticleDetailsViewController.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

class ArticleDetailsViewController: UIViewController {
    private var article: Article!
    var delegate: FavouriteAddDelegate?
    
    @IBOutlet private weak var articleImage: UIImageView!
    @IBOutlet private weak var articleSource: UILabel!
    @IBOutlet private weak var articleTitle: UILabel!
    @IBOutlet private weak var articleAuthor: UILabel!
    @IBOutlet private weak var articleDate: UILabel!
    @IBOutlet private weak var articleAbstract: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = article.image {
            articleImage.image = image
        } else {
            loadImage()
        }
        
        articleSource.text = article.source
        articleTitle.text = article.title
        articleAuthor.text = article.author
        articleDate.text = article.date
        articleAbstract.text = article.abstract
    }
    
    func articleToShow(_ article: Article, showAddToFavouritesButton: Bool = false) {
        self.article = article
        ArticlesManager.lastAdded = nil
        
        if showAddToFavouritesButton {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: "Add to favourites", style: .plain,
                target: self, action: #selector (addFavouritesButtonPressed(_:))
            )
        }
    }
    
    private func loadImage() {
        guard let bestImage = chooseBestMedia(media: article.media),
              let url = URL(string: bestImage.url) else {
            return
        }
        
        let networkManager = NetworkManager(url: url)
        networkManager.obtainImage { [weak self] result in
            switch result {
            case .success(let image):
                self?.articleImage.image = image
            case .failure(_):
                break
            }
        }
    }
    
    private func chooseBestMedia(media: [Media]) -> MediaMetadata? {
        let packsOfImages = media.filter { $0.type == "image" }
        
        var bestImages = [MediaMetadata?]()
        for images in packsOfImages {
            bestImages.append(images.metadatas.max { $0.height*$0.width < $1.height*$1.width })
        }
        
        return bestImages.compactMap { $0 }.max { $0.height*$0.width < $1.height*$1.width }
    }
    
    @objc private func addFavouritesButtonPressed(_ sender: Any) {
        navigationItem.rightBarButtonItem?.isEnabled = false
        delegate?.wasAddedToFavourites(article: article)
        ArticlesManager.save(article: article)
    }
}
